http://test1.addgoals.com/academy/academy/

http://test2.addgoals.com/academy/academy/

Использует базы test1 и test2 соответсвенно. Нету возможности конфигурировать разные хосты, все на одном хосте(т.е. один докер контейнер с несколькими базами). Не самое скейлабл решение, но для тестирования подойдет. 

Для локального тестирования:
cat /etc/hosts

127.0.0.1   test1.addgoals.com
127.0.0.1   test2.addgoals.com

При первом запуске заходим в контейнер веба и для каждой базы выполняем 

/usr/bin/odoo --load base,web --addons-path modules --dev reload -i base -d <db_name> --db_host db -w odoo_password -r odoo_user

Где db_name in (test1, test2, default_db)