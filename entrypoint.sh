#!/usr/bin/env bash

until nc -w 1 -z db 5432; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
  echo 'sleeping'
done
sleep 1
>&2 echo "Postgres is up - executing command"

/usr/bin/odoo --load base,web --addons-path modules --dev reload -i base -c /etc/odoo/odoo.conf
