FROM python:3.6

# install odoo
RUN apt-get update && apt-get install -y netcat vim

RUN pip install -U setuptools

RUN wget -O - https://nightly.odoo.com/odoo.key | apt-key add - && \
 echo "deb http://nightly.odoo.com/13.0/nightly/deb/ ./" >> /etc/apt/sources.list.d/odoo.list && \
 apt-get update && apt-get install -y odoo

COPY config/odoo.conf /etc/odoo


COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

WORKDIR oddy

COPY . /oddy/
